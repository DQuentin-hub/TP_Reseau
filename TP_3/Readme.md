# TP3 : systemd

## 0. Prérequis

On creer une nouvelle VM sous centos 7 puis on la pack.

```
$ vagrant package --output b2-tp3-centos.box
$ vagrant box add b2-tp3-centos b2-tp3-centos.box

```

## I. Services systemd

### 1. Intro

> On affiche le nombre de systemd disponible sur la machine:

```
[vagrant@tp3 ~]$ sudo systemctl list-unit-files --type=service | grep -c systemd
51
```

> On affiche le nombre de systeme actif sur la machine:

```
[vagrant@tp3 ~]$ sudo systemctl -t service | grep -c systemd
13
```

> On affiche le nombre de services systemd qui ont échoué ("failed") ou qui sont inactifs ("exited") sur la machine

```
[vagrant@tp3 ~]$ systemctl list-units  | grep systemd | egrep -c "exited|failed"
10
```

> On affiche la liste des services systemd qui démarrent automatiquement au boot ("enabled")

```
[vagrant@tp3 ~]$ systemctl list-unit-files  | grep systemd | grep "enabled"
systemd-readahead-collect.service             enabled
systemd-readahead-drop.service                enabled
systemd-readahead-replay.service              enabled
```

### 2. Analyse d'un service

```
[vagrant@tp3 ~]$ systemctl status nginx.service
● nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Mon 2020-10-05 09:42:28 UTC; 1min 51s ago
  Process: 2234 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  Process: 2232 ExecStartPre=/usr/sbin/nginx -t (code=exited, status=0/SUCCESS)
  Process: 2230 ExecStartPre=/usr/bin/rm -f /run/nginx.pid (code=exited, status=0/SUCCESS)
 Main PID: 2236 (nginx)
   CGroup: /system.slice/nginx.service
           ├─2236 nginx: master process /usr/sbin/nginx
           └─2237 nginx: worker process
```

> Avec la commande on eput voir que le path de nginx est "/usr/lib/systemd/system/nginx.service"


```
[vagrant@tp3 ~]$ systemctl cat nginx
# /usr/lib/systemd/system/nginx.service
[Unit]
Description=The nginx HTTP and reverse proxy server
After=network.target remote-fs.target nss-lookup.target

[Service]
Type=forking
PIDFile=/run/nginx.pid
# Nginx will fail to start if /run/nginx.pid already exists but has the wrong
# SELinux context. This might happen when running `nginx -t` from the cmdline.
# https://bugzilla.redhat.com/show_bug.cgi?id=1268621
ExecStartPre=/usr/bin/rm -f /run/nginx.pid
ExecStartPre=/usr/sbin/nginx -t
ExecStart=/usr/sbin/nginx
ExecReload=/bin/kill -s HUP $MAINPID
KillSignal=SIGQUIT
TimeoutStopSec=5
KillMode=process
PrivateTmp=true

[Install]
WantedBy=multi-user.target
```

> Afficher son contenu et expliquer les lignes qui comportent :
  * `ExecStart`:
>Commandes avec leurs arguments qui sont exécutées au démarrage de ce service.
  * `ExecStartPre`:
>Commandes supplémentaires exécutées avant ou après la commande dans ExecStart
  * `PIDFile`:
>Prend un chemin faisant référence au fichier PID du service.
  * `Type`:
>Configure le type de démarrage du processus pour cette unité de service.
  * `ExecReload`:
>Commandes à exécuter pour déclencher un rechargement de configuration dans le service.
  * `Description`:
> Affiche la descrition basique du programme
  * `After`:
> Ceux-ci garantissent que les unités de service normales effectuent l'initialisation de base du système et sont correctement terminées avant l'arrêt du système.




* On liste tout les services qui contiennent la ligne "WantedBy=multi-user.target"
```
[vagrant@tp3 system]$ grep -lr "WantedBy=multi-user.target" | grep service
rpcbind.service
rdisc.service
tcsd.service
sshd.service
rhel-configure.service
rsyslog.service
irqbalance.service
cpupower.service
crond.service
rpc-rquotad.service
wpa_supplicant.service
chrony-wait.service
chronyd.service
NetworkManager.service
ebtables.service
gssproxy.service
tuned.service
firewalld.service
nfs-server.service
rsyncd.service
nginx.service
vmtoolsd.service
postfix.service
auditd.service
```

## 3. Création d'un service

### A. Serveur web

> On creer le user et le group web
```
[vagrant@tp3 system]$ sudo useradd web -M -s /sbin/nologin
```

> On modifie le fichier visudo pour accoder les droits sudo sans mdp au groupe web
```
[vagrant@tp3 ~]$ sudo visudo

%web    ALL=(ALL)    NOPASSWD: ALL
```

> On creer un fichier pour le service du serveur web

```
[vagrant@tp3 ~]$ cd /etc/systemd/system/
[vagrant@tp3 system]$ sudo touch serverweb.service
```

> On modifie le fichier pour faire fonctionner le service
```
[vagrant@tp3 system]$ sudo vim serverweb.service
[Unit]
Description=Serveur web sous python


[Service]
Type=simple

User=web
Environment=PORT=8080

ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=${PORT}/tcp
ExecStart=/usr/bin/python2 -m SimpleHTTPServer ${PORT}
ExecStopPost=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=${PORT}/tcp

Restart=Always

[Install]
WantedBy=multi-user.target
```

> Puis on lance le service et on verifie son fonctionnement

```
[vagrant@tp3 system]$ sudo systemctl daemon-reload
[vagrant@tp3 system]$ sudo systemctl start firewalld
[vagrant@tp3 system]$ sudo systemctl start serverweb.service 
[vagrant@tp3 system]$ sudo systemctl status serverweb.service
● serverweb.service - Serveur web sous python
   Loaded: loaded (/etc/systemd/system/serverweb.service; disabled; vendor preset: disabled)
   Active: active (running) since Wed 2020-10-07 09:02:52 UTC; 7s ago
  Process: 2492 ExecStopPost=/usr/bin/sudo /usr/bin/firewall-cmd --remove-port=${PORT}/tcp (code=exited, status=252)
  Process: 2620 ExecStartPre=/usr/bin/sudo /usr/bin/firewall-cmd --add-port=${PORT}/tcp (code=exited, status=0/SUCCESS)
 Main PID: 2627 (python2)
   CGroup: /system.slice/serverweb.service
           └─2627 /usr/bin/python2 -m SimpleHTTPServer 8080

Oct 07 09:02:51 tp3.vagrant systemd[1]: Starting Serveur web sous python...
Oct 07 09:02:51 tp3.vagrant sudo[2620]:      web : TTY=unknown ; PWD=/ ; USER=root ; COMMAND=/usr/bin/firewall-cmd --add-port=8080/tcp
Oct 07 09:02:52 tp3.vagrant systemd[1]: Started Serveur web sous python.
```

> On recupere l'IP de la machine puis on se connecte via le navigateur de notre PC.

```
[vagrant@tp3 system]$ ip a
[...]
4: eth2: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:f8:a1:bc brd ff:ff:ff:ff:ff:ff
    inet 172.28.128.54/24 brd 172.28.128.255 scope global noprefixroute dynamic eth2
       valid_lft 879sec preferred_lft 879sec
    inet6 fe80::a00:27ff:fef8:a1bc/64 scope link
       valid_lft forever preferred_lft forever
```

![](https://i.imgur.com/cWbm9tU.png)

### B. Sauvegarde

>Script de variable et fonction
```bash
[vagrant@tp3 opt]$ sudo cat backup_variable.sh
#!/bin/bash
# Simple backup script

## VARIABLES

# Colors :D
declare -r NC="\e[0m"
declare -r B="\e[1m"
declare -r RED="\e[31m"
declare -r GRE="\e[32m"

# Target directory : the one we want to backup
declare -r target_path="${1}"
declare -r target_dirname=$(awk -F'/' '{ print $NF }' <<< "${target_path%/}")

# Craft the backup full path and name
declare -r backup_destination_dir="/opt/backup/"
declare -r backup_date="$(date +%y%m%d_%H%M%S)"
declare -r backup_filename="${target_dirname}_${backup_date}.tar.gz"
declare -r backup_destination_path="${backup_destination_dir}/${backup_filename}"

# Informations about the User that must run this script
declare -r backup_user_name="backup"
declare -ri backup_user_uid=1002
declare -ri backup_user_umask=077

# The quantity of backup to keep for each directory
declare -i backups_quantity=7
declare -ri backups_quantity=$((backups_quantity+1))

## FUNCTIONS

# Get timestamp in order to log
get_current_timestamp() {
  timestamp=$(date "+[%h %d %H:%M:%S]")
}

# Echo arguments with a timestamp
log() {
  log_level="${1}"
  log_message="${2}"

  get_current_timestamp

  if [[ "${log_level}" == "ERROR" ]]; then
    echo -e "${timestamp} ${B}${RED}[ERROR]${NC} ${log_message}" >&2

  elif [[ "${log_level}" == "INFO" ]]; then
    echo -e "${timestamp} ${B}[INFO]${NC} ${log_message}"

  fi
}

# Craft an archive, compress it, and store it in $backup_destination_dir
archive_and_compress() {

  dir_to_backup="${1}"

  log "INFO" "Starting backup."

  # Actually creates the compressed archive
  tar cvzf \
    "${backup_destination_path}" \
    "${dir_to_backup}" \
    --ignore-failed-read &> /dev/null

  # Test if the archive has been created successfully
  if [[ $? -eq 0 ]]
  then
    log "INFO" "${B}${GRE}Success.${NC} Backup ${backup_filename} has been saved to ${backup_destination_dir}."
  else
    log "ERROR" "Backup ${backup_filename} has failed."

    # Even if tar has failed, it creates the archive, so we remove it in case of failure
    rm -f "${backup_destination_path}"

    exit 1
  fi
}

# Delete oldest backups, eg only keep the $backups_quantity most recent backups, for a given directory
delete_oldest_backups() {

  # Get list of oldest backups
  # BE CAREFUL : this only works if there's no IFS character in file names (space, tabs, newlines, etc.)
  oldest_backups=$(ls -tp "${backup_destination_dir}" | grep -v '/$' | grep -E "^${target_dirname}.*$" | tail -n +${backups_quantity})

  if [[ ! -z $oldest_backups ]]
  then

    log "INFO" "This script only keep the ${backups_quantity} most recent backups for a given directory."

    for backup_to_del in ${oldest_backups}
    do
      # This line might be buggy if file names contain IFS characters
      rm -f "${backup_destination_dir}/${backup_to_del}" &> /dev/null

      if [[ $? -eq 0 ]]
      then
        log "INFO" "${B}${GRE}Success.${NC} Backup ${backup_to_del} has been removed from ${backup_destination_dir}."
      else
        log "[ERROR]" "Deletion of backup ${backup_to_del} from ${backup_destination_dir} has failed."
        exit 1
      fi

    done
  fi
}
```

>Script de test 

```bash
[vagrant@tp3 opt]$ sudo cat backup_test.sh
#!/bin/bash
source /opt/backup_variable.sh

## PREFLIGHT CHECKS

# Force a specific user to run te script
if [[ ${EUID} -ne ${backup_user_uid} ]]; then
  log "ERROR" "This script must be run as \"${backup_user_name}\" user, which UID is ${backup_user_uid}. Exiting."
  exit 1
fi

# Check that the target dir actually exists and is readable
if [[ ! -d "${target_path}" ]]; then
  log "ERROR" "The target path ${target_path} does not exist. Exiting."
  exit 1
fi
if [[ ! -r "${target_path}" ]]; then
  log "ERROR" "The target path ${target_path} is not readable. Exiting."
  exit 1
fi

# Check that the destination dir actually exists ans is writable
if [[ ! -d "${backup_destination_dir}" ]]; then
  log "ERROR" "The destination dir ${backup_destination_dir} does not exist. Exiting."
  exit 1
fi
if [[ ! -w "${backup_destination_dir}" ]]; then
  log "ERROR" "The destination dir ${backup_destination_dir} is not writable. Exiting."
  exit 1
fi

exit 0
```


>Script de sauvegarde principal:

```bash
[vagrant@tp3 opt]$ sudo cat backup.sh
#!/bin/bash
source /opt/backup_variable.sh

### CODE

# Set the backup user UMASK
umask ${backup_user_umask}

# Backup the site
archive_and_compress "${target_path}"

```

>Script de verification du nombre de backup

```bash
[vagrant@tp3 opt]$ sudo cat backup.sh
#!/bin/bash
source /opt/backup_variable.sh

### CODE

# Set the backup user UMASK
umask ${backup_user_umask}

# Backup the site
archive_and_compress "${target_path}"

[vagrant@tp3 opt]$ sudo cat backup_verif.sh
#!/bin/bash
source /opt/backup_variable.sh

### CODE

# Set the backup user UMASK
umask ${backup_user_umask}

# Rotate backups (only keep the most recent ones)
delete_oldest_backups

exit 0
```

>Script du service de sauvegarde:

```bash
[vagrant@tp3 system]$ cat save.service
[Unit]
Description=Execution des scripts de sauvegarde


[Service]
Type=oneshot

User=backup
Environment=SITE1=/srv/site1
Environment=SITE2=/srv/site2

ExecStartPre=/opt/backup_test.sh ${SITE1}
ExecStartPre=/opt/backup_test.sh ${SITE2}
ExecStart=/opt/backup.sh ${SITE1}
ExecStart=/opt/backup.sh ${SITE2}
ExecStopPost=/opt/backup_verif.sh

[Install]
WantedBy=multi-user.target
```

> Script du service timer qui execute le service toute les heures

```bash
[vagrant@tp3 system]$ cat backup.timer 
[Unit]
Description=Execute save toutes les heures

[Timer]
OnCalendar=*-*-* *:00:00
Unit=save.service

[Install]
WantedBy=multi-user.target
```

> Verification du fonctionnement du timer:

```bash
[vagrant@tp3 system]$ systemctl status backup.timer
● backup.timer - Execute save toutes les heures
   Loaded: loaded (/etc/systemd/system/backup.timer; disabled; vendor preset: disabled)
   Active: active (waiting) since Fri 2020-10-09 07:40:15 UTC; 15s ago
[vagrant@tp3 system]$ systemctl list-timers
NEXT                         LEFT       LAST                         PASSED    UNIT                         ACTIVATES
Fri 2020-10-09 08:00:00 UTC  19min left n/a                          n/a       backup.timer                 save.service
Sat 2020-10-10 07:23:44 UTC  23h left   Fri 2020-10-09 07:23:44 UTC  16min ago systemd-tmpfiles-clean.timer systemd-tmpfiles-clean.service

2 timers listed.
Pass --all to see loaded but inactive timers, too.
```


## II. Autres features


### 1. Gestion de boot

> On effectue la commande:

```
systemd-analyze plot
```

> On recupere le contenu sur notre machine hote puis on le place dans un fichier svg. Puis on l'ouvre avec un navigateur web:

![](https://i.imgur.com/mJcoFr6.png)

> Les 3 services les plus lents à demarré sont:

- tuned.service (3.193s)
- sssd.service (1.709s)
- polkit.service (781ms)

### 2. Gestion de l'heure

> On effectue la commande demandé:
```
[vagrant@tp3 ~]$ timedatectl
               Local time: Fri 2020-10-09 08:01:36 UTC
           Universal time: Fri 2020-10-09 08:01:36 UTC
                 RTC time: Fri 2020-10-09 08:01:37
                Time zone: UTC (UTC, +0000)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
```

> Le fuseau horaire de la VM est UTC +0
> La machine est connecté a un serveur NTP

>On change le fuseau horaire:

```
[vagrant@tp3 ~]$ sudo timedatectl set-timezone Europe/Paris
[vagrant@tp3 ~]$ timedatectl
               Local time: Fri 2020-10-09 10:14:13 CEST
           Universal time: Fri 2020-10-09 08:14:13 UTC
                 RTC time: Fri 2020-10-09 08:14:14
                Time zone: Europe/Paris (CEST, +0200)
System clock synchronized: yes
              NTP service: active
          RTC in local TZ: no
```

### 3. Gestion des noms et de la résolution de noms


> On effectue la commande demandé:
```
[vagrant@tp3 ~]$ hostnamectl
   Static hostname: tp3.2.vagrant
         Icon name: computer-vm
           Chassis: vm
        Machine ID: a218776cefd74d96ab547dc33ee6b691
           Boot ID: a846e7c06d0e47988c8406f6c46ef7e4
    Virtualization: oracle
  Operating System: CentOS Linux 8 (Core)
       CPE OS Name: cpe:/o:centos:centos:8
            Kernel: Linux 4.18.0-80.el8.x86_64
      Architecture: x86-64
```

> Le hostname de la machine est tp3.2.vagrant

> On change le hostname:

```
[vagrant@tp3 ~]$ sudo hostnamectl set-hostname tp3.2.3.vagrant
[vagrant@tp3 ~]$ hostnamectl
   Static hostname: tp3.2.3.vagrant
         Icon name: computer-vm
           Chassis: vm
        Machine ID: a218776cefd74d96ab547dc33ee6b691
           Boot ID: a846e7c06d0e47988c8406f6c46ef7e4
    Virtualization: oracle
  Operating System: CentOS Linux 8 (Core)
       CPE OS Name: cpe:/o:centos:centos:8
            Kernel: Linux 4.18.0-80.el8.x86_64
      Architecture: x86-64
```

