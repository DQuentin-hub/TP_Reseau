# TP2 : Déploiement automatisé

# 0. Prérequis

## Install Vagrant

## Init Vagrant



# I. Déploiement simple

### creation d'un vagrant file 

```bash
Vagrant.configure("2")do|config|
  config.vm.box="centos/7"

  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
  config.vbguest.auto_update = false

  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
  config.vm.box_check_update = false 

  # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
  config.vm.synced_folder ".", "/vagrant", disabled: true


  # modifie IP de la VM en statique
  config.vm.network "public_network" ,ip:"192.168.2.11"
    
  # change le hostname de la VM
  config.vm.hostname = "tp2.vagrant"

  # change le nom de la VM sur vagrant
  config.vm.define "TP2_Vagrant"
  
  # Modifie le nom de la VM virtualbox puis la RAM de la VM
  config.vm.provider "virtualbox" do |vb|
    vb.customize [
                    "modifyvm", :id,
                    "--name", "TP2_Vagrant",
                    "--memory", "1024"
                  ]
  end

end
```

### Verification de la configuration

>Verification du hostname:

```shell
[vagrant@tp2 ~]$ hostname
tp2.vagrant
```

>Verification de l'IP:
```shell
[vagrant@tp2 ~]$ ip a
[...]
3: eth1: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 08:00:27:93:7b:39 brd ff:ff:ff:ff:ff:ff
    inet 192.168.2.11/24 brd 192.168.2.255 scope global noprefixroute eth1
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fe93:7b39/64 scope link
       valid_lft forever preferred_lft forever
```

>Verifiaction du nom de la VM:
```shell
PS C:\Users\Quent\vagrant> vagrant status
Current machine states:

TP2_Vagrant               running (virtualbox)
```

>Verification de la RAM:
```shell
[vagrant@tp2 ~]$ free -m
              total        used        free      shared  buff/cache   available
Mem:            990          84         814           6          91         787
Swap:          2047           0        2047
```
🌞 Modifier le `Vagrantfile`

### ajout d'un script qui ce lance au demarage de la machine et install vim

>Ajout de cette ligne dans le Vagrantfile
```shell
  # Exécution d'un script au démarrage de la VM
  config.vm.provision "shell", path: "script.sh"
```

>Contenu du fichier script.sh

```bash
#!/usr/bin/env bash

sudo yum install -y vim
```

>Resultat
```shell
$ vagrant up
Bringing machine 'TP2_Vagrant' up with 'virtualbox' provider...
==> TP2_Vagrant: Importing base box 'centos/7'...
==> TP2_Vagrant: Matching MAC address for NAT networking...
==> TP2_Vagrant: Setting the name of the VM: vagrant_TP2_Vagrant_1601369129182_15854
==> TP2_Vagrant: Clearing any previously set network interfaces...
==> TP2_Vagrant: Preparing network interfaces based on configuration...
    TP2_Vagrant: Adapter 1: nat
    TP2_Vagrant: Adapter 2: bridged
==> TP2_Vagrant: Forwarding ports...
    TP2_Vagrant: 22 (guest) => 2222 (host) (adapter 1)
==> TP2_Vagrant: Running 'pre-boot' VM customizations...
==> TP2_Vagrant: Booting VM...
==> TP2_Vagrant: Waiting for machine to boot. This may take a few minutes...
    TP2_Vagrant: SSH address: 127.0.0.1:2222
    TP2_Vagrant: SSH username: vagrant
    TP2_Vagrant: SSH auth method: private key
    TP2_Vagrant: 
    TP2_Vagrant: Vagrant insecure key detected. Vagrant will automatically replace
    TP2_Vagrant: this with a newly generated keypair for better security.
    TP2_Vagrant: 
    TP2_Vagrant: Inserting generated public key within guest...
    TP2_Vagrant: Removing insecure key from the guest if it's present...
    TP2_Vagrant: Key inserted! Disconnecting and reconnecting using new SSH key...
==> TP2_Vagrant: Machine booted and ready!
==> TP2_Vagrant: Checking for guest additions in VM...
    TP2_Vagrant: No guest additions were detected on the base box for this VM! Guest
    TP2_Vagrant: additions are required for forwarded ports, shared folders, host only
    TP2_Vagrant: networking, and more. If SSH fails on this machine, please install
    TP2_Vagrant: the guest additions and repackage the box to continue.
    TP2_Vagrant:
    TP2_Vagrant: This is not an error message; everything may continue to work properly,
    TP2_Vagrant: in which case you may ignore this message.
==> TP2_Vagrant: Setting hostname...
==> TP2_Vagrant: Configuring and enabling network interfaces...
==> TP2_Vagrant: Running provisioner: shell...
    TP2_Vagrant: Running: C:/Users/Quent/AppData/Local/Temp/vagrant-shell20200929-7536-1crea70.sh
[...]
    TP2_Vagrant: ---> Package vim-enhanced.x86_64 2:7.4.629-6.el7 will be installed
[...]
    TP2_Vagrant: Complete!

Quent@LAPTOP-IB6K1IJD MINGW64 ~/vagrant
$ vagrant ssh
[vagrant@tp2 ~]$ cd /home/vagrant/
[vagrant@tp2 ~]$ mkdir test
[vagrant@tp2 ~]$ cd test/
[vagrant@tp2 test]$ touch file_test
[vagrant@tp2 test]$ vim file_test
[vagrant@tp2 test]$
```

### ajout d'un disque supplementaire de 5Go


>Modification du Vagrant file:
```shell
CONTROL_NODE_DISK = './tmp/large_disk.vdi'

Vagrant.configure("2")do|config|
  config.vm.box="b2-tp2-centos"

[...]
  config.vm.provider "virtualbox" do |vb|
    # Crée le disque, uniquement s'il nexiste pas déjà
    unless File.exist?(CONTROL_NODE_DISK)
      vb.customize ['createhd', '--filename', CONTROL_NODE_DISK, '--variant', 'Fixed', '--size', 5120]
    end

    # Attache le disque à la VM
    vb.customize ['storageattach', :id,  '--storagectl', 'IDE', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', CONTROL_NODE_DISK]
  end

end
```

>Verification de l'ajout du disque a la VM

```shell
[vagrant@tp2 ~]$ lsblk
NAME   MAJ:MIN RM SIZE RO TYPE MOUNTPOINT
sda      8:0    0  40G  0 disk 
└─sda1   8:1    0  40G  0 part /
sdb      8:16   0   5G  0 disk
```

# II. Re-package

>Packaging de la VM sous le nom b2-tp2-centos

```shell
Quent@LAPTOP-IB6K1IJD MINGW64 ~/vagrant
$ vagrant package --output b2-tp2-centos.box
==> TP2_Vagrant: Attempting graceful shutdown of VM...
==> TP2_Vagrant: Clearing any previously set forwarded ports...
==> TP2_Vagrant: Exporting VM...
==> TP2_Vagrant: Compressing package to: C:/Users/Quent/vagrant/b2-tp2-centos.box

Quent@LAPTOP-IB6K1IJD MINGW64 ~/vagrant
$ vagrant box add b2-tp2-centos b2-tp2-centos.box
==> box: Box file was not detected as metadata. Adding it directly...
==> box: Adding box 'b2-tp2-centos' (v0) for provider: 
    box: Unpacking necessary files from: file://C:/Users/Quent/vagrant/b2-tp2-centos.box
    box:
==> box: Successfully added box 'b2-tp2-centos' (v0) for 'virtualbox'!

Quent@LAPTOP-IB6K1IJD MINGW64 ~/vagrant
$ vagrant box list
b2-tp2-centos (virtualbox, 0)
centos/7      (virtualbox, 2004.01)
```

# III. Multi-node deployment

>On effectue le deploiment de deux VM qui ce nomme node1 et node2 avec le meme Vagrantfile

```bash
Vagrant.configure("2") do |config|
  # Configuration commune à toutes les machines
  config.vm.box = "b2-tp2-centos"

  # Config une première VM "node1.tp2.b2"
  config.vm.define "node1" do |node1|
    # remarquez l'utilisation de 'node1.' défini sur la ligne au dessus
    # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
    node1.vbguest.auto_update = false
    # Désactive les updates auto qui peuvent ralentir le lancement de la machine
    node1.vm.box_check_update = false 
    # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
    node1.vm.synced_folder ".", "/vagrant", disabled: true
    node1.vm.network "private_network", ip: "192.168.2.21"
    node1.vm.hostname = "node1.tp2.b2"
    node1.vm.provider "virtualbox" do |vb|
      vb.customize [
                    "modifyvm", :id,
                    "--name", "node1.tp2.b2",
                    "--memory", "1024"
                  ]
      end
  
  end

  # Config une première VM "node2.tp2.b2"
  config.vm.define "node2" do |node2|
    # remarquez l'utilisation de 'node2.' défini sur la ligne au dessus
    # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
    node2.vbguest.auto_update = false
    # Désactive les updates auto qui peuvent ralentir le lancement de la machine
    node2.vm.box_check_update = false 
    # La ligne suivante permet de désactiver le montage d'un dossier partagé (ne marche pas tout le temps directement suivant vos OS, versions d'OS, etc.)
    node2.vm.synced_folder ".", "/vagrant", disabled: true
    node2.vm.network "private_network", ip: "192.168.2.22"
    node2.vm.hostname = "node2.tp2.b2"
    node2.vm.provider "virtualbox" do |vb|
      vb.customize [
                    "modifyvm", :id,
                    "--name", "node2.tp2.b2",
                    "--memory", "512"
                  ]
      end
  end
end
```
# IV. Automation here we (slowly) come

>Verification du fonctionnement des VM.

```bash
Quent@LAPTOP-IB6K1IJD MINGW64 ~/vagrant/part4
$ vagrant up
Bringing machine 'node1' up with 'virtualbox' provider...
Bringing machine 'node2' up with 'virtualbox' provider...

Quent@LAPTOP-IB6K1IJD MINGW64 ~/vagrant/part4
$ vagrant ssh node2
Last login: Tue Sep 29 08:46:54 2020 from 10.0.2.2
[vagrant@node2 ~]$ curl node1.tp2.b2
<html>
<head><title>301 Moved Permanently</title></head>
<body>
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx/1.16.1</center>
</body>
</html>
[vagrant@node2 ~]$ curl https://node1.tp2.b2
<html>
<head><title>301 Moved Permanently</title></head>
<body>
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx/1.16.1</center>
</body>
</html>
[vagrant@node2 ~]$ curl -L https://node1.tp2.b2/site1
<h1>Hello from site 1</h1>
[vagrant@node2 ~]$ curl -L https://node1.tp2.b2/site2
<h1>Hello from site 2</h1>
```

