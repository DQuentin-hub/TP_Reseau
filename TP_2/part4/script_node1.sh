#!/usr/bin/env bash

setenforce 0
systemctl start firewalld
firewall-cmd --add-port=80/tcp --permanent
firewall-cmd --add-port=443/tcp --permanent
firewall-cmd --reload

useradd admin -m
usermod -aG wheel admin

echo '192.168.1.12 node2 node2.tp2.b2' | tee /etc/hosts


useradd web -M -s /sbin/nologin
openssl req -new -newkey rsa:2048 -days 365 -nodes -x509 -subj "/CN=node1.tp2.b2" -keyout node1.tp2.b2.key -out node1.tp2.b2.crt

mv /home/vagrant/node1.tp2.b2.key /etc/pki/tls/private/node1.tp2.b2.key
chmod 400 /etc/pki/tls/private/node1.tp2.b2.key
chown web:web /etc/pki/tls/private/node1.tp2.b2.key

mv /home/vagrant/node1.tp2.b2.crt /etc/pki/tls/certs/node1.tp2.b2.crt
chmod 444 /etc/pki/tls/certs/node1.tp2.b2.crt
chown web:web /etc/pki/tls/certs/node1.tp2.b2.crt

mkdir /srv/site1
mkdir /srv/site2
touch /srv/site1/index.html
touch /srv/site2/index.html
echo '<h1>Hello from site 1</h1>' | tee /srv/site1/index.html
echo '<h1>Hello from site 2</h1>' | tee /srv/site2/index.html
chown web:web /srv/site1 -R
chown web:web /srv/site2 -R
chmod 700 /srv/site1 /srv/site2
chmod 400 /srv/site1/index.html /srv/site2/index.html



echo '
worker_processes 1;
error_log nginx_error.log;
pid /run/nginx.pid;
user web;

events {
    worker_connections 1024;
}

http {
    server {
        listen 80;
        server_name node1.tp2.b2;
            
        location / {
            return 301 /site1;
        }

        location /site1 {
            alias /srv/site1;
        }

        location /site2 {
            alias /srv/site2;
        }
    }
    server {
        listen 443 ssl;

        server_name node1.tp2.b2;
        ssl_certificate /etc/pki/tls/certs/node1.tp2.b2.crt;
        ssl_certificate_key /etc/pki/tls/private/node1.tp2.b2.key;
            
        location / {
            return 301 /site1;
        }

        location /site1 {
            alias /srv/site1;
        }

            location /site2 {
                alias /srv/site2;
            }
        }
    }


'> /etc/nginx/nginx.conf

systemctl start nginx