#!/usr/bin/env bash

setenforce 0
systemctl start firewalld
firewall-cmd --reload

useradd admin -m
usermod -aG wheel admin

echo '192.168.1.11 node1 node1.tp2.b2' | tee /etc/hosts

echo -n | openssl s_client -connect node1.tp2.b2:443 \
    | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > /etc/pki/ca-trust/source/anchors/node1.tp2.b2.cert
    update-ca-trust